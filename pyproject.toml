[build-system]
requires = [
    "hatchling",
    "hatch-vcs",
]
build-backend = "hatchling.build"

[project]
name = "artemis_sg"
authors = [
  { name="John Duarte", email="john@yeliad.us" },
]
description = "Package for generating Google slide decks"
readme = "README.md"
requires-python = ">=3.7"
license = "GPL-3.0-or-later"
classifiers = [
    "Development Status :: 4 - Beta",
    "Environment :: Console",
    "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3 :: Only",
    "Operating System :: OS Independent",
]
dependencies = [
    "google-cloud-storage",
    "google-api-python-client",
    "google-auth-httplib2",
    "google_auth_oauthlib",
    "selenium",
    "isbnlib",
    "pillow",
    "puremagic",
    "click",
    "openpyxl",
    "platformdirs",
    "tomli >= 1.1.0 ; python_version < '3.11'",
    "tomli_w",
    "flatten_dict",
    "rich",
    "amazoncaptcha",
    "beautifulsoup4",
]
dynamic = ["version"]

[project.urls]
"Homepage" = "https://gitlab.com/johnduarte/artemis_slide_generator"
"Bug Tracker" = "https://gitlab.com/johnduarte/artemis_slide_generator/-/issues"
"Changelog" = "https://gitlab.com/johnduarte/artemis_slide_generator/blob/main/CHANGELOG.md"

[project.optional-dependencies]
docs = [
  "furo",
  "myst_parser >=0.13",
  "sphinx >=4.0",
  "sphinx-autobuild",
  "sphinx-copybutton",
  "sphinx-inline-tabs",
  "sphinx-autodoc-typehints",
  "sphinxcontrib-mermaid",
  "sphinx-click",
]
test = [
    "pytest",
    "pytest-cov",
    "black",
    "flake8",
    "isort",
    "ruff",
]

[tool.pytest.ini_options]
addopts = [
    "--import-mode=importlib",
]
optional-tests = [
  "no_integration",
]

[project.scripts]
artemis_sg = "artemis_sg.cli:cli"

[tool.hatch.version]
source = "vcs"

[tool.hatch.version.raw-options]
version_scheme = "python-simplified-semver"
local_scheme = "no-local-version"
git_describe_command = ["git", "describe", "--dirty", "--tags", "--long", "--match", "v*"]

[tool.hatch.build.hooks.vcs]
version-file = "src/artemis_sg/_version.py"

[tool.hatch.envs.default]
features = [
    "docs",
    "test",
]

[tool.hatch.envs.default.scripts]
api-doc = "sphinx-apidoc -o docs/api --module-first --no-toc --force src/artemis_sg"
doc-build = "sphinx-build --keep-going -n -T -b html docs public"
doc = [
  "api-doc",
  "doc-build",
]
test = "pytest --doctest-modules src --doctest-continue-on-failure -m \"not integration\" {args:tests}"
test-cov = "coverage run -m pytest -vv --doctest-modules src --doctest-continue-on-failure -m \"not integration\" {args:tests}"
cov-report = [
  "- coverage combine",
  "coverage report",
]
cov = [
  "test-cov",
  "cov-report",
]
cov-html = [
  "cov",
  "coverage html -i"
]
lint = [
  "ruff format --check .",
  "ruff check .",
]
format = [
  "ruff format ."
]

[tool.isort]
profile = "black"

[tool.black]
line-length = 88

[tool.ruff]
line-length = 88
lint.pylint.max-args = 6
lint.select = [
  "A",
  "B",
  "C",
  "DTZ",
  "E",
  "EM",
  "F",
  "FBT",
  "I",
  "ICN",
  "ISC",
  "N",
  "PLC",
  "PLE",
  "PLR",
  "PLW",
  "Q",
  "RUF",
  "S",
  "SIM",
  "T",
  "TID",
  "UP",
  "W",
  "YTT",
]
lint.ignore = ["ISC001"]

[tool.ruff.lint.isort]
known-first-party = ["artemis_sg"]

[tool.ruff.lint.flake8-tidy-imports]
ban-relative-imports = "all"

[tool.coverage.run]
branch = true
source_pkgs = ["artemis_sg"]

[tool.coverage.paths]
artemis_sg = ["src/artemis_sg"]
tests = ["tests"]

[tool.coverage.report]
exclude_lines = [
  "no cov",
  "if __name__ == .__main__.:",
]
