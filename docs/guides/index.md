# Topical Guides

```{toctree}
:titlesonly:
development_setup
powershell_profile
```

These pages are intended to address particular use cases that do not fit
into the topic of tutorial.


## Indices and tables

- {ref}`genindex`
- {ref}`modindex`
- {ref}`search`

<!-- prettier-ignore-start -->
[powershell profile]:            powershell_profile.md
<!-- prettier-ignore-end -->
